package com.hatimhaddy.truckonline.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hatimhaddy.truckonline.R;

public class GridItemAdapter extends BaseAdapter {

    private Context context;
    private final Integer[] mThumbIds;
    private final String[] mThumbTxt;

    public GridItemAdapter(Context context, Integer[] mThumbIds, String[] mThumbTxt) {
        this.context = context;
        this.mThumbIds = mThumbIds;
        this.mThumbTxt = mThumbTxt;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(context);
            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.grid_item, null);
            // set image based on selected text
            ImageView imageView = (ImageView) gridView.findViewById(R.id.grid_item_image);
            imageView.setImageResource(mThumbIds[position]);
            TextView textView = (TextView) gridView.findViewById(R.id.grid_item_txt);
            textView.setText(mThumbTxt[position]);
        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }

    @Override
    public int getCount() {
        return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
