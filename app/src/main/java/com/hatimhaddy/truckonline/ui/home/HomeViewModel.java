package com.hatimhaddy.truckonline.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.hatimhaddy.truckonline.repository.driver.DriverRepository;
import com.hatimhaddy.truckonline.repository.models.pojo.DriverStatus;
import com.hatimhaddy.truckonline.repository.models.pojo.UserProfile;

public class HomeViewModel extends ViewModel {

    private LiveData<UserProfile> driver;
    private LiveData<DriverStatus> driverStatus;
    DriverRepository driverRepository;

    public HomeViewModel() {
        super();
    }

    public void init(){
        driverRepository = new DriverRepository();
        driverRepository.getUserProfile();;
        driverRepository.getDriverStatus();

        driver = driverRepository.getUserProfileLiveData();
        driverStatus = driverRepository.getDriverStatusLiveData();
    }

    public LiveData<UserProfile> getDriver() {
        return driver;
    }
    public LiveData<DriverStatus> getDriverStatus() {
        return driverStatus;
    }
}