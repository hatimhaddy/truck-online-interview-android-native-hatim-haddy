package com.hatimhaddy.truckonline.ui.missions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.hatimhaddy.truckonline.R;

public class MissionsFragment extends Fragment {

    private MissionsViewModel missionsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        missionsViewModel =
                ViewModelProviders.of(this).get(MissionsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_missions, container, false);
        final TextView textView = root.findViewById(R.id.txt_missions);
        missionsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}