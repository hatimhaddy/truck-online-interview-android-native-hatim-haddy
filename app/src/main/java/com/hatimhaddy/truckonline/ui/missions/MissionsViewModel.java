package com.hatimhaddy.truckonline.ui.missions;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MissionsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MissionsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Missions");
    }

    public LiveData<String> getText() {
        return mText;
    }
}