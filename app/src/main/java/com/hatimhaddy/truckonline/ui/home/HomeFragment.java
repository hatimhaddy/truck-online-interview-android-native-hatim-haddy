package com.hatimhaddy.truckonline.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.hatimhaddy.truckonline.R;
import com.hatimhaddy.truckonline.repository.models.pojo.DriverStatus;
import com.hatimhaddy.truckonline.repository.models.pojo.UserProfile;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public Integer[] mThumbIds = {
            R.drawable.ic_grid_1, R.drawable.ic_grid_2, R.drawable.ic_grid_3,
            R.drawable.ic_grid_4, R.drawable.ic_grid_5, R.drawable.ic_grid_6,
            R.drawable.ic_grid_7
    };

    public String[] mThumbTxt = {
            "Saisie Carburant", "Gestion palette", "Espace Restant",
            "Scan QRCode", "Mes documents", "Réglages application",
            "Gestion temps bus"
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.init();
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        final TextView txtView_driverShortName = root.findViewById(R.id.txt_driver_shortname);
        homeViewModel.getDriver().observe(getViewLifecycleOwner(), new Observer<UserProfile>() {
            @Override
            public void onChanged(@Nullable UserProfile driver) {
                if(driver != null){
                    txtView_driverShortName.setText(driver.getShortName());
                }
            }
        });

        final TextView txtView_driverVehicleVRN = root.findViewById(R.id.txt_driver_vehiclevrn);
        homeViewModel.getDriverStatus().observe(getViewLifecycleOwner(), new Observer<DriverStatus>() {
            @Override
            public void onChanged(@Nullable DriverStatus driverStatus) {
                if(driverStatus != null){
                    txtView_driverVehicleVRN.setText(driverStatus.getVehicleVRN());
                }
            }
        });

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GridView gridView = (GridView) view.findViewById(R.id.gridview_services);
        gridView.setAdapter(new GridItemAdapter(view.getContext(),mThumbIds, mThumbTxt));
    }
}