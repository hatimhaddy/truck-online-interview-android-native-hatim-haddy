package com.hatimhaddy.truckonline.webservice;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static final String BASE_URL = "http://ec2-34-252-249-185.eu-west-1.compute.amazonaws.com";
    private static APIClient instance = null;

    private Webservice webservice;

    private APIClient() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

        Retrofit.Builder builder
                = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        this.webservice = retrofit.create(Webservice.class);
    }

    public static APIClient getInstance() {
        if (instance == null) {
            instance = new APIClient();
        }
        return instance;
    }

    public Webservice getWebservice() {
        return this.webservice;
    }
}
