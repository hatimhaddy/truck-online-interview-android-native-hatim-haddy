package com.hatimhaddy.truckonline.webservice;

import com.hatimhaddy.truckonline.repository.models.pojo.DriverActivity;
import com.hatimhaddy.truckonline.repository.models.pojo.DriverStatus;
import com.hatimhaddy.truckonline.repository.models.pojo.UserProfile;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Webservice {

    @GET("apis/interview/1/users/profile")
    Call<UserProfile> getUserProfile();

    @GET("apis/interview/1/status")
    Call<DriverStatus> getDriverStatus();

    @GET("apis/interview/1/activities")
    Call<List<DriverActivity>> getDriverActivities();
}
