package com.hatimhaddy.truckonline.repository.models.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverStatus {

    @SerializedName("vehicleVRN")
    @Expose
    private String vehicleVRN;

    @SerializedName("trailerVRN")
    @Expose
    private String trailerVRN;

    @SerializedName("activity")
    @Expose
    private DriverActivity activity;

    public String getVehicleVRN() {
        return vehicleVRN;
    }

    public void setVehicleVRN(String vehicleVRN) {
        this.vehicleVRN = vehicleVRN;
    }

    public String getTrailerVRN() {
        return trailerVRN;
    }

    public void setTrailerVRN(String trailerVRN) {
        this.trailerVRN = trailerVRN;
    }

    public DriverActivity getActivity() {
        return activity;
    }

    public void setActivity(DriverActivity activity) {
        this.activity = activity;
    }
}
