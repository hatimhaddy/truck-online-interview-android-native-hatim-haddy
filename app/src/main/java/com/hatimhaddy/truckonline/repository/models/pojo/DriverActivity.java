package com.hatimhaddy.truckonline.repository.models.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverActivity {

    @SerializedName("vehicleVIN")
    @Expose
    private String vehicleVIN;

    @SerializedName("startDate")
    @Expose
    private String startDate;

    @SerializedName("endDate")
    @Expose
    private String endDate;

    @SerializedName("drivingSeat")
    @Expose
    private String drivingSeat;

    @SerializedName("activity")
    @Expose
    private String activity;

    @SerializedName("source")
    @Expose
    private String source;

    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("startKm")
    @Expose
    private int startKm;

    @SerializedName("endKm")
    @Expose
    private int endKm;

    @SerializedName("tag")
    @Expose
    private String tag;

    public String getVehicleVIN() {
        return vehicleVIN;
    }

    public void setVehicleVIN(String vehicleVIN) {
        this.vehicleVIN = vehicleVIN;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDrivingSeat() {
        return drivingSeat;
    }

    public void setDrivingSeat(String drivingSeat) {
        this.drivingSeat = drivingSeat;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getStartKm() {
        return startKm;
    }

    public void setStartKm(int startKm) {
        this.startKm = startKm;
    }

    public int getEndKm() {
        return endKm;
    }

    public void setEndKm(int endKm) {
        this.endKm = endKm;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
