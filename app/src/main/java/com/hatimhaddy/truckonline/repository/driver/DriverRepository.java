package com.hatimhaddy.truckonline.repository.driver;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.hatimhaddy.truckonline.repository.models.pojo.DriverActivity;
import com.hatimhaddy.truckonline.repository.models.pojo.DriverStatus;
import com.hatimhaddy.truckonline.repository.models.pojo.UserProfile;
import com.hatimhaddy.truckonline.webservice.APIClient;
import com.hatimhaddy.truckonline.webservice.Webservice;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverRepository {

    Webservice webservice = APIClient.getInstance().getWebservice();
    MutableLiveData<UserProfile> userProfileMutableLiveData;
    MutableLiveData<DriverStatus> driverStatusMutableLiveData;
    MutableLiveData<List<DriverActivity>> listDriverActivitiesMutableLiveData;

    public DriverRepository(){
        userProfileMutableLiveData = new MutableLiveData<>();
        driverStatusMutableLiveData = new MutableLiveData<>();
    }

    public void getUserProfile(){
        Call<UserProfile> userProfileCall = webservice.getUserProfile();
        userProfileCall.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                userProfileMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                userProfileMutableLiveData.postValue(null);
            }
        });
    }

    public LiveData<UserProfile> getUserProfileLiveData(){
        return userProfileMutableLiveData;
    }


    public void getDriverStatus(){
        Call<DriverStatus> driverStatusCall = webservice.getDriverStatus();
        driverStatusCall.enqueue(new Callback<DriverStatus>() {
            @Override
            public void onResponse(Call<DriverStatus> call, Response<DriverStatus> response) {
                driverStatusMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<DriverStatus> call, Throwable t) {
                driverStatusMutableLiveData.postValue(null);
            }
        });
    }

    public LiveData<DriverStatus> getDriverStatusLiveData() {
        return driverStatusMutableLiveData;
    }


    public void getDriverActivities(){
        Call<List<DriverActivity>> listDriverActivitiesCall = webservice.getDriverActivities();
        listDriverActivitiesCall.enqueue(new Callback<List<DriverActivity>>() {
            @Override
            public void onResponse(Call<List<DriverActivity>> call, Response<List<DriverActivity>> response) {
                listDriverActivitiesMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<List<DriverActivity>> call, Throwable t) {
                listDriverActivitiesMutableLiveData.postValue(null);
            }
        });
    }

    public LiveData<List<DriverActivity>> getListDriverActivitiesLiveData() {
        return listDriverActivitiesMutableLiveData;
    }
}
